import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/docs",
    name: "docs",
    component: () =>
      import( "../views/docs.vue")
  },
  {
    path: "/setting.vue",
    name: "setting",
    component: () =>
      import( "../views/setting.vue")
  },
  {
    path: "/feedback.vue",
    name: "feedback",
    component: () =>
      import( "../views/feedback.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
